require 'yaml'
require 'pp'

file = ARGV[0] #YAMLファイル
node_add_frist = ARGV[1] #ノード名の最初 
node_add_last = ARGV[2] #ノード名の最後
node_name_add_frist = ARGV[3] #node_nameの最初
node_name_add_last = ARGV[4] #node_nameの最後

data = open(file,'r') { |f| YAML.load(f) } #入力変換

data['node_groups'].transform_keys! do |key|
    node_add_frist + key + node_add_last
end

data['node_groups'].each_with_index do |node_group, i|
    #後でeachにして書き直す
    #↓のcontents[0]をcontents[i]にしても想定通り動かない iの中は0,1,2 後で調べる
    for i in 0..4 do
        if !node_group[1]['contents'][i].nil? 
            if !node_group[1]['contents'][i]['carousel'].nil? #carouselが含まれている場合のみ実行       
                for num in 0..node_group[1]['contents'][i]['carousel']['columns'].length - 1 do #actionの数だけ繰り返し
                    if node_group[1]['contents'][i]['carousel']['columns'][num]['action'].has_key?("dest")
                        node_group[1]['contents'][i]['carousel']['columns'][num]['action']['dest'] = node_add_frist + node_group[1]['contents'][i]['carousel']['columns'][num]['action']['dest'] + node_add_last
                    end 
                end
            end
            if pp !node_group[1]['contents'][i]['imageCarousel'].nil? #imageCarouselが含まれている場合のみ実行       
                for num in 0..node_group[1]['contents'][i]['imageCarousel']['columns'].length - 1 do #actionの数だけ繰り返し
                    if node_group[1]['contents'][i]['imageCarousel']['columns'][num]['action'] 
                        if node_group[1]['contents'][i]['imageCarousel']['columns'][num]['action'].has_key?("dest")
                            node_group[1]['contents'][i]['imageCarousel']['columns'][num]['action']['dest'] = node_add_frist + node_group[1]['contents'][i]['imageCarousel']['columns'][num]['action']['dest'] + node_add_last
                        end
                    end 
                end
            end
            if !node_group[1]['contents'][i]['buttonBubble'].nil? #buttonBubbleが含まれている場合のみ実行          
                for num in 0..node_group[1]['contents'][i]['buttonBubble']['actions'].length - 1 do #actionsの数だけ繰り返し
                    if node_group[1]['contents'][i]['buttonBubble']['actions'][num].has_key?('dest') #destが含まれている場合のみ実行
                        node_group[1]['contents'][i]['buttonBubble']['actions'][num]['dest'] = node_add_frist + node_group[1]['contents'][i]['buttonBubble']['actions'][num]['dest'] + node_add_last
                    end
                end
            end
            if !node_group[1]['contents'][i]['node_name'].nil? #node_nameが含まれている場合のみ実行   
                node_group[1]['contents'][i]['node_name'] = node_name_add_frist + node_group[1]['contents'][i]['node_name'] + node_name_add_last
            end
        end
    end
end

YAML.dump(data, File.open(file, 'w')) #出力
